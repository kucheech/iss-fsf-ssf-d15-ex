(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q"];

    function MyAppService($http, $q) {
        var service = this;

        //expose the following services
        service.getCustomers = getCustomers;
        service.getCustomerById = getCustomerById;
        service.addCustomer = addCustomer;
        service.updateCustomer = updateCustomer;

        function getCustomers() {
            var defer = $q.defer();

            $http.get("/customers")
                .then(function (result) {
                    // console.log(result);
                    var customers = result.data;
                    defer.resolve(customers);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getCustomerById(id) {
            var defer = $q.defer();

            $http.get("/customer/" + id)
                .then(function (result) {
                    var customer = result.data;
                    defer.resolve(customer);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function addCustomer(customer) {
            console.log("addCustomer");
            var defer = $q.defer();
            $http.post("/customers", { customer: customer })
                .then(function (result) {
                    // var customer = result.data;
                    console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function updateCustomer(customer) {
            // console.log("updateCustomer");
            var defer = $q.defer();
            const id = customer.CUSTOMER_ID;
            $http.put("/customer/" + id, { customer: customer })
                .then(function (result) {
                    // var customer = result.data;
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

    }

})();