(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["MyAppService"];

    function MyAppCtrl(MyAppService) {
        var self = this; // vm
        self.id = "";
        self.customers = [];
        self.customer = null;
        self.detailViewMode = false;
        self.editMode = false;
        self.copy = null;
        self.addMode = false;
        self.showSuccessfulAdd = false;
        self.previous = 0;
        self.next = 0;

        self.init = function () {
            MyAppService.getCustomers()
                .then(function (result) {
                    self.customers = result;
                }).catch(function (e) {
                    console.log(e);
                });
        };

        self.init();

        self.enableDetailViewMode = function (id) {
            // console.log(id);
            MyAppService.getCustomerById(id)
                .then(function (result) {
                    self.customer = result;
                    self.detailViewMode = true;
                    // setPrevNext();
                }).catch(function (e) {
                    console.log(e);
                    self.detailViewMode = false;
                });
        }

        self.enableEditMode = function () {
            self.copy = JSON.parse(JSON.stringify(self.customer)); //make copy
            self.editMode = true;
        }

        self.saveEdit = function () {

            if (self.addMode) {
                MyAppService.addCustomer(self.customer)
                    .then(function (result) {
                        // self.customer = result;
                        // self.detailViewMode = true;
                        if (result.status == 201) {
                            self.showSuccessfulAdd = true;
                        }
                        self.addMode = false;
                        self.editMode = false;
                    }).catch(function (e) {
                        console.log(e);
                        // self.detailViewMode = false;
                        self.addMode = false;
                        self.editMode = false;
                    });

            } else {
                MyAppService.updateCustomer(self.customer)
                    .then(function (result) {
                        // self.customer = result;
                        // self.detailViewMode = true;
                        self.editMode = false;
                    }).catch(function (e) {
                        // console.log(e);
                        // self.detailViewMode = false;
                        self.editMode = false;
                    });

            }


        }

        self.cancelEditMode = function () {
            self.customer = self.copy;
            self.editMode = false;
        }

        self.enableAddMode = function () {
            self.addMode = true;
            self.editMode = true;
            self.customer = {
                NAME: "",
                ADDRESSLINE1: "",
                ADDRESSLINE2: "",
                CITY: "",
                STATE: "",
                ZIP: "",
                PHONE: "",
                FAX: "",
                EMAIL: "",
                CREDIT_LIMIT: 0,
                DISCOUNT_CODE: ""
            }
            self.detailViewMode = true;
        }

        self.viewPrevious = function () {
            var i = parseInt(getIndex(self.customer));
            if (i > 0) {
                var id = self.customers[i - 1].CUSTOMER_ID;
                self.enableDetailViewMode(id);
            }
        }


        self.viewNext = function () {
            var i = parseInt(getIndex(self.customer)); //have to typecast it to int to prevent the + bug
            if (i > 0 && i < self.customers.length-1) {
                var id = self.customers[i + 1].CUSTOMER_ID;
                self.enableDetailViewMode(id);
            }
        }

        // function setPrevNext() {
        //     var i = getIndex(self.customer);
        //     console.log(i);
        //     if (i > 0) {
        //         self.previous = self.customers[i - 1].CUSTOMER_ID;
        //     }

        //     if (i > 0 && i < (self.customers.length - 1)) {
        //         self.next = self.customers[i + 1].CUSTOMER_ID;
        //     }

        // }

        function getIndex(customer) {
            for (var i in self.customers) {
                if (customer.CUSTOMER_ID == self.customers[i].CUSTOMER_ID) {
                    return i;
                }
            }

            return -1;
        }

    }

})();