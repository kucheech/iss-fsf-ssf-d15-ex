"use strict";
const request = require('supertest');
const app = require('../server/app');

/* test root path */
describe('Test the root path', function() {
    test('It should response the GET method', function() {
        return request(app).get("/").then(function(response) {
            expect(response.statusCode).toBe(200);
        })
    });
});

/** test no end point found */
describe('Test no end point found', function() {
    test('It return page not found in response', function() {
        return request(app).get("/sdffdf").then(function(response) {
            expect(response.statusCode).toBe(404);
        })
    });
});

/** test /friends endpoint */
describe('Test /friends endpoint', function() {
    test('It should return an array', function() {
        return request(app).get("/friends").then(function(response) {
            expect(response.statusCode).toBe(200);
            expect(response.type).toBe("application/json");
            var array = response.body;
            // console.log(JSON.stringify(response.body));
            expect(response.body.length).toBe(4);
        })
    });
});


/** test /numfriends endpoint */
describe('Test /numfriends endpoint', function() {
    test('It should return a value 4', function() {
        return request(app).get("/numfriends").then(function(response) {
            expect(response.statusCode).toBe(200);
            expect(response.text).toBe("4");
            // console.log(response);
        })
    });
});