const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

var pool = mysql.createPool({
    connectionLimit: 10,
    host: '127.0.0.1',
    user: 'user',
    password: 'password',
    database: 'derby_sample'
});

const SELECT_FILM_BY_FILM_ID = "select * from film where film_id = ?";
const SELECT_CUSTOMERS = "select * from customer";
const SELECT_CUSTOMER_BY_ID = "select * from customer where customer_id = ?";
const UPDATE_CUSTOMER_BY_ID = "update customer set name = ?, addressline1 = ?, addressline2 = ?, city = ?,state = ?,zip = ?,phone = ?,fax = ?,email = ?,credit_limit = ?,discount_code = ? where customer_id = ?";
const INSERT_CUSTOMER = "insert into customer (customer_id,name,addressline1,addressline2,city,state,zip,phone,fax,email,credit_limit,discount_code) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
const SELECT_MAX_CUSTOMER_ID = "select max(customer_id) as n from customer";


const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

//native promise
// const mkQuery = function (sql, pool) {
//     const sqlQuery = function () {
//         var sqlParams = [];
//         for (i in arguments) {
//             sqlParams.push(arguments[i]);
//         }

//         const promise = new Promise(function (resolve, reject) {
//             pool.getConnection(function (err, conn) {
//                 if (err) {
//                     reject(err);
//                     return;
//                 }

//                 conn.query(sql, sqlParams, function (err, result) {
//                     if (err) {
//                         reject(err);
//                     } else {
//                         resolve(result);
//                     }

//                     conn.release();
//                 });
//             });
//         });

//         return promise;
//     }

//     return sqlQuery;
// };

const findFilmByFilmId = mkQuery(SELECT_FILM_BY_FILM_ID, pool);
const getCustomers = mkQuery(SELECT_CUSTOMERS, pool);
const getCustomerById = mkQuery(SELECT_CUSTOMER_BY_ID, pool);
const updateCustomerById = mkQuery(UPDATE_CUSTOMER_BY_ID, pool);
const addCustomer = mkQuery(INSERT_CUSTOMER, pool);
const getMaxCustomerId = mkQuery(SELECT_MAX_CUSTOMER_ID, pool);


app.get("/customers", function (req, res) {

    getCustomers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No customers");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/customer/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    // console.log(filmId);

    getCustomerById(id)
        .then(function (result) {
            // console.log(result);
            if (result.length > 0) {
                var customer = result[0];
                res.status(200);
                res.format({
                    "application/json": function () {
                        res.json(customer);
                    },
                    "text/plain": function () {
                        res.send("Name: " + customer.name);
                    },
                    "default": function () {
                        // log the request and respond with 406
                        res.status(406).send('Not Acceptable');
                    }
                });

            } else {
                res.status(404).type("text/plain").send("Customer not found");
            }
        }).catch(function (err) {
            handleError(err, res);
        });
});

app.post("/customers", function (req, res) {

    var customer = req.body.customer;
    console.log(customer);
    getMaxCustomerId().then(function (result) {
        // console.log(result[0].n);
        const id = result[0].n + 1;
        console.log(id);
        addCustomer(id, customer.NAME, customer.ADDRESSLINE1, customer.ADDRESSLINE2, customer.CITY, customer.STATE, customer.ZIP, customer.PHONE, customer.FAX, customer.EMAIL, customer.CREDIT_LIMIT, customer.DISCOUNT_CODE)
            .then(function (result) {
                console.log(result);
                res.status(201).end();
            }).catch(function (err) {
                handleError(err, res);
            });
    }).catch(function (err) {
        handleError(err, res);
    });

    // console.log(customer);
    // updateCustomerById(customer.NAME, customer.ADDRESSLINE1, customer.ADDRESSLINE2, customer.CUSTOMER_ID)
    //     .then(function (result) {
    //         console.log(result);
    //         // res.status(404).type("text/plain").send("Customer not found");
    //         res.status(200).end();
    //     }).catch(function (err) {
    //         handleError(err, res);
    //     });
});


app.put("/customer/:id", function (req, res) {
    const id = parseInt(req.params.id); //to convert type 
    if (isNaN(id) || id < 0) {
        res.status(400).type("text/plain").send("id should be a (positive) number");
        return;
    }

    const customer = req.body.customer;
    // console.log(customer);
    updateCustomerById(customer.NAME, customer.ADDRESSLINE1, customer.ADDRESSLINE2, customer.CITY, customer.STATE, customer.ZIP, customer.PHONE, customer.FAX, customer.EMAIL, customer.CREDIT_LIMIT, customer.DISCOUNT_CODE, customer.CUSTOMER_ID)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});

app.get("/film/:filmId", function (req, res) {
    const filmId = parseInt(req.params.filmId); //to convert type 
    if (isNaN(filmId) || filmId < 0) {
        res.status(400).type("text/plain").send("filmId should be a (positive) number");
        return;
    }

    // console.log(filmId);

    findFilmByFilmId(filmId)
        .then(function (result) {
            // console.log(result);
            if (result.length > 0) {
                var film = result[0];
                res.status(200);
                res.format({
                    "application/json": function () {
                        res.json(film);
                    },
                    "text/plain": function () {
                        res.send("Title: " + film.title);
                    },
                    "default": function () {
                        // log the request and respond with 406
                        res.status(406).send('Not Acceptable');
                    }
                });

            } else {
                res.status(404).type("text/plain").send("Film not found");
            }
        }).catch(function (err) {
            handleError(err, res);
        });
});


app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});

app.get("/films", function (req, res) {

    var deferred = q.defer();

    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            deferred.reject(res.status(500).send(err));
        }

        const SQL_QUERY = "SELECT * FROM film";
        conn.query(SQL_QUERY, function (err, rows, fields) {
            if (err) {
                conn.release();
                console.log(err);
                deferred.reject(res.status(500).send(err));
                throw err;
            }

            console.log(rows);
            // console.log(fields);

            deferred.resolve(res.status(200).send(rows));
            conn.release();
        });
    });

    return deferred.promise;
});

app.get("/users", function (req, res) {
    // console.log("/users");
    var deferred = q.defer();

    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release();
            deferred.reject(res.status(500).send(err));
        }

        const SQL_QUERY = "SELECT * FROM users";
        conn.query(SQL_QUERY, function (err, rows, fields) {
            if (err) {
                conn.release();
                console.log(err);
                deferred.reject(res.status(500).send(err));
                throw err;
            }

            console.log(rows);
            // console.log(fields);

            deferred.resolve(res.status(200).send(rows));
            conn.release();
        });
    });

    return deferred.promise;
});

app.get("/users/:userId", function (req, res) {
    console.log(req.params.userId);
    // console.log("/users");
    var deferred = q.defer();

    // pool.getConnection(function (err, conn) {
    //     if (err) {
    //         conn.release();
    //         deferred.reject(res.status(500).send(err));
    //     }

    //     const SQL_QUERY = "SELECT * FROM users";
    //     conn.query(SQL_QUERY, function (err, rows, fields) {
    //         if (err) {
    //             conn.release();
    //             console.log(err);
    //             deferred.reject(res.status(500).send(err));
    //             throw err;
    //         }

    //         console.log(rows);
    //         // console.log(fields);

    //         deferred.resolve(res.status(200).send(rows));
    //         conn.release();
    //     });
    // });

    // return deferred.promise;
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app